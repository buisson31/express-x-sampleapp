
import express from 'express'
import inquirer from 'inquirer'
import bcrypt from 'bcrypt'

import expressX from '@jcbuisson/express-x'
// import expressX from '../src/express-x.mjs'

import { hashPassword } from '@jcbuisson/express-x-common-hooks'



/////////////////      EXPRESS-X APP      /////////////////

const app = expressX(express())

app.createDatabaseService('user', { client: 'prisma' })
app.service('user').hooks({
   before: {
      create: [hashPassword],
   },
})

async function main() {
   try {
      const answers = await inquirer.prompt([
         {
            name: 'pseudo',
            type: 'string',
            message: 'Enter admin pseudo',
            default: 'admin',
         },
         {
            name: 'password',
            type: 'password',
            message: 'Enter password',
         },
         {
            name: 'repeat',
            type: 'password',
            message: 'Repeat password',
         },
      ])
      // check password match
      if (answers.password !== answers.repeat) {
         throw new Error("*** passwords do not match")
      }

      const admin = await app.service('user').create({
         pseudo: answers.pseudo,
         password: answers.password,
         name: "Admin",
         role: 'admin',
      })
      console.log(`admin user ${admin.pseudo} created successfully`)
   } catch(err) {
      console.error("*** error", err.toString())
   } finally {
      process.exit(0)
   }
}

main()
