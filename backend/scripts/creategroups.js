
import express from 'express'
import inquirer from 'inquirer'

import expressX from '@jcbuisson/express-x'


/////////////////      EXPRESS-X APP      /////////////////

const app = expressX(express())

app.createDatabaseService('group', { client: 'prisma' })

async function main() {
   try {
      const answers = await inquirer.prompt([
         {
            name: 'name1',
            type: 'string',
            message: 'Enter group #1 name',
            default: 'Gryffondor',
         },
         {
            name: 'name2',
            type: 'string',
            message: 'Enter group #2 name',
            default: 'Serpentard',
         },
      ])

      await app.service('group').create({
         name: answers.name1,
      })
      await app.service('group').create({
         name: answers.name2,
      })
      console.log("groups created successfully")
   } catch(err) {
      console.error("*** error", err.toString())
   } finally {
      process.exit(0)
   }
}

main()
