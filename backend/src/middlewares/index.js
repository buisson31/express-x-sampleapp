
import axios from 'axios'


function oauthMiddleware(app) {

   // this route is called after google sign-in dialog
   return async function (req, res, next) {

      // get authorization code
      const code = req.query.code
      // exchange this code for tokens
      try {
         const { data } = await axios.post('https://oauth2.googleapis.com/token', {
            code,
            client_id: '1040254190260-kuui0br7lk5vj0bmibn2m6p1etu6sqig.apps.googleusercontent.com',
            client_secret: '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',
            redirect_uri: 'http://localhost:3030/oauth/google',
            grant_type: 'authorization_code',
         })
         console.log('oauthMiddleware data', data)

         // get user info
         const tokenFromGoogle = data.access_token
         const urlForGettingUserInfo = 'https://www.googleapis.com/oauth2/v2/userinfo'
         const { data: userData } = await axios.get(urlForGettingUserInfo, {
            headers: {
               Authorization: `Bearer ${tokenFromGoogle}`
            },
         })

         // upsert user in database, using google id
         const user = await app.service('user').upsert({
            where: {
               google_id: userData.id,
            },
            create: {
               google_id: userData.id,
               name: userData.name,
               role: 'user',
            },
            update: {
               name: userData.name,
            },
          })
         

         res.send(user)
      } catch(err) {
         res.status(400).send(err.toString())
      }
   }
}

export default function (app) {
   // `redirect_uri` provided in the google signin configuration
   app.use('/oauth/google', oauthMiddleware(app))
}
