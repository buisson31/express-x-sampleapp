
import express from 'express'
import bodyParser from 'body-parser'
import expressX from '@jcbuisson/express-x'
// import expressX from './express-x.mjs'

import middlewares from './middlewares/index.js'
import services from './services/index.js'
import channels from './channels/index.js'

// `app` is also a regular express application
const app = expressX(express())

// body parsers for http requests
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// middlewares
app.configure(middlewares)

// services
app.configure(services)

// pub/sub
app.configure(channels)

app.server.listen(3030, () => console.log('App listening at http://localhost:3030'))
