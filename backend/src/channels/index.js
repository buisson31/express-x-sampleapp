
export default function(app) {

   // subscribe to channel 'anonymous' on connection
   // see 'channelSubscription' hook for subscriptions on signin
   app.on('connection', (connection) => {
      console.log('connection', connection.id)
      app.joinChannel('anonymous', connection)
   })

   // publish
   app.service('user').publish(async (user, context) => {
      return ['anonymous']
   })
   app.service('authenticate').publish(async (user, context) => {
      return ['anonymous']
   })

}
