
import hooks from './user.hooks.js'

export default function (app) {

   app.createDatabaseService('user', { client: 'prisma' })

   // HTTP endpoints
   app.addHttpRestRoutes('/api/user', app.service('user'))


   app.service('user').hooks(hooks)

}
