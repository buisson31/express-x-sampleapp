
// const hashPassword = require('../../../hooks/hash-password.hook')
// const isAuthenticated = require('../../../hooks/is-authenticated.hook')
// const prevent = require('../../../hooks/prevent.hook')
import { hashPassword, isAuthenticated, prevent } from '@jcbuisson/express-x-common-hooks'

export default {
   before: {
      create: [hashPassword],
      // all: [isAuthenticated],
   },
   after: {
      all: [prevent('password')]
   }
}
