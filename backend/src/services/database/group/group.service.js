
import hooks from './group.hooks.js'

export default function (app) {

   app.createDatabaseService('group', { client: 'prisma' })

   // HTTP endpoints
   app.addHttpRestRoutes('/api/group', app.service('group'))

   // hooks
   app.service('group').hooks(hooks)
}
