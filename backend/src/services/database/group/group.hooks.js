
// const isAuthenticated = require('../../../hooks/is-authenticated.hook')
import { isAuthenticated } from '@jcbuisson/express-x-common-hooks'

export default {
   before: {
      all: [isAuthenticated],
   },
}
