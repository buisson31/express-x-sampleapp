
import config from 'config'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

import hooks from './authenticate.hooks.js'


export default function (app) {

   const prisma = app.get('prisma') // BOF...

   app.createService('authenticate', {
      
      verifyCredentials: async (data) => {
         console.log('authenticate check', data)
         try {
            const authConfig = config.authentication
            const entity = authConfig.entity
            const usernameField = authConfig[data.strategy].usernameField
            const passwordField = authConfig[data.strategy].passwordField
            // check if a user exists with this username
            const where = {}
            where[usernameField] = data.username
            const user = await prisma[entity].findUnique({ where })
            console.log('auth user', user)
            if (user) {
               // user exists; check password
               const match = await bcrypt.compare(data.password, user[passwordField])
               if (match) {
                  const accessToken = jwt.sign({ sub: user.id }, authConfig.secret, authConfig.jwtOptions)
                  return {
                     accessToken,
                     user,
                  }
               } else {
                  // incorrect password
                  return {
                     error: "Incorrect credentials",
                  }
               }
            } else {
               // no user with this username in database
               return {
                  error: "Incorrect credentials",
               }
            }
         } catch(err) {
            // unknown error
            return {
               error: err.toString(),
            }
         }
      },

      reauthenticate: async (accessToken) => {
         console.log("reauthentication", accessToken)
         try {
            const payload = jwt.verify(accessToken, config.authentication.secret)
            return ({
               accessToken,
               payload,
            })
         } catch(err) {
            return {
               error: "*** reauthentication error: invalid access token",
            }
         }
      }
   })

   app.service('authenticate').hooks(hooks)

}
