
// const setSessionJWT = require('../../../hooks/set-session-jwt.hook')
import { setSessionJWT } from '@jcbuisson/express-x-common-hooks'

export default {
   after: {
      all: [
         setSessionJWT,
         // channelSubscription,
      ]
   }
}
