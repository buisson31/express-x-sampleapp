
import nodemailer from 'nodemailer'
import config from 'config'


export default function (app) {

   app.createService('mail', {
      create: ({ from, to, subject, text }) => {
         console.log('send mail', from, to, subject, text)
         try {
            const transporter = nodemailer.createTransport(config.NODEMAILER)
            return transporter.sendMail({
               from,
               to,
               subject,
               text,
               html: text,
            })
         } catch(err) {
            return {
               error: err.toString()
            }
         }
      },
   })
}
