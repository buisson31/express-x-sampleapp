
import jwt from 'jsonwebtoken'

export default function (app) {

   app.createService('invitation', {
      
      send: async ({ group, email }) => {
         console.log('send invitation', group, email)

         // create signed jwt, encoding groupId
         const token = jwt.sign(group, "MYSECRET")
         const text = `<a href="http://localhost:3000/signup?token=${token}">Cliquez ici</a> pour créer un compte et rejoindre le groupe ${group.name}`

         // send email
         await app.service('mail').create({
            from: "dumbledore@poudlard.fr",
            to: email,
            subject: `Invitation à rejoindre le groupe ${group.name}`,
            text,
         })
         return {
            // used with 'after' hook
            accessToken,
         }
      },
   })

}
