
// database services
import userService from './database/user/user.service.js'
import groupService from './database/group/group.service.js'

// custom services
import authenticateService from './custom/authenticate/authenticate.service.js'
import invitationService from './custom/invitation/invitation.service.js'
import mailService from './custom/mail/mail.service.js'


export default function (app) {

   // add database services
   app.configure(userService)
   app.configure(groupService)

   // add custom services
   app.configure(authenticateService)
   app.configure(invitationService)
   app.configure(mailService)
}
