
import { computed } from "vue"
import { useSessionStorage } from '@vueuse/core'

import app from '/src/app-client'

// groups state backed in SessionStorage

const initialState = () => ({
   // Linus Borg dictionnary
   groups: {},
   
   groupListReady: false,
})

const stateGroups = useSessionStorage('state-groups', initialState())

const resetUseGroups = () => {
   stateGroups.value = initialState()
}


app.service('group').on('create', group => {
   console.log('groupS EVENT created', group)
   stateGroups.value.groups[group.id] = group
})

app.service('group').on('patch', group => {
   console.log('groupS EVENT patched', group)
   stateGroups.value.groups[group.id] = group
})

app.service('group').on('remove', group => {
   console.log('groupS EVENT removed', group)
   delete stateGroups.value.groups[group.id]
})


const groupOfId = computed(() => (groupId) => {
   let value = stateGroups.value.groups[groupId]
   if (value === undefined) {
      app.service('group').get(groupId)
      .then(group => { stateGroups.value.groups[group.id] = group })
      .catch(() => {})
   }
   return value
})

const groupsList = computed(() => {
   if (!stateGroups.value.groupListReady) {
      app.service('group').find({})
      .then(groupList => {
         groupList.forEach(group => { stateGroups.value.groups[group.id] = group })
         stateGroups.value.groupListReady = true
      })
      .catch(() => {})
      return []
   }
   return Object.values(stateGroups.value.groups)
})

export function useGroups() {
   return {
      stateGroups,
      resetUseGroups,
      groupOfId,
      groupsList,
   }
}
