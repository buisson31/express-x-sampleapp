
import { computed } from "vue"
import { useSessionStorage } from '@vueuse/core'

import app from '/src/app-client'

// users state backed in SessionStorage

const initialState = () => ({
   // Linus Borg dictionnary
   users: {},
   
   userListReady: false,
})

const stateUsers = useSessionStorage('state-users', initialState())

const resetUseUsers = () => {
   stateUsers.value = initialState()
}


app.service('user').on('create', user => {
   console.log('USERS EVENT created', user)
   stateUsers.value.users[user.id] = user
})

app.service('user').on('patch', user => {
   console.log('USERS EVENT patched', user)
   stateUsers.value.users[user.id] = user
})

app.service('user').on('remove', user => {
   console.log('USERS EVENT removed', user)
   delete stateUsers.value.users[user.id]
})


const userOfId = computed(() => (userId) => {
   let value = stateUsers.value.users[userId]
   if (value === undefined) {
      app.service('user').get(userId)
      .then(user => {
         stateUsers.value.users[user.id] = user
      })
      .catch(() => {})
   }
   return value
})

const userName = computed(() => (userId) => {
   const user = userOfId.value(userId)
   return user ? user.name : ''
})

const usersList = computed(() => {
   if (!stateUsers.value.userListReady) {
      app.service('user').find({})
      .then(userList => {
         userList.forEach(user => { stateUsers.value.users[user.id] = user })
         stateUsers.value.userListReady = true
      })
      .catch(() => {})
      return []
   }
   return Object.values(stateUsers.value.users)
})

export function useUsers() {
   return {
      stateUsers,
      resetUseUsers,
      userOfId,
      userName,
      usersList,
   }
}
