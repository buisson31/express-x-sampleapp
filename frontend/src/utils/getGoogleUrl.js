
export const getGoogleUrl = (from) => {
   const rootUrl = `https://accounts.google.com/o/oauth2/v2/auth`
 
   const options = {
      redirect_uri: "http://localhost:3030/oauth/google",
      client_id: '1040254190260-kuui0br7lk5vj0bmibn2m6p1etu6sqig.apps.googleusercontent.com',
      access_type: 'offline',
      response_type: 'code',
      prompt: 'consent',
      scope: [
         'https://www.googleapis.com/auth/userinfo.profile',
         'https://www.googleapis.com/auth/userinfo.email',
      ].join(' '),
      state: from,
   }
 
   const qs = new URLSearchParams(options)
 
   return `${rootUrl}?${qs.toString()}`
}
