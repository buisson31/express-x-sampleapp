
import io from 'socket.io-client'
import expressxClient from '@jcbuisson/express-x-client'
// import expressxClient from './express-x-client.mjs'


const socket = io({ transports: ["websocket"] }) // with no argument, connection is made with the server which served index.html

const app = expressxClient(socket)

app.setConnectionCallback((connectionId) => {
   const accessToken = window.sessionStorage.getItem('feathers-jwt')
   if (accessToken) {
      // disconnect/reconnect: reauthenticate
      console.log('reauthenticate with', accessToken)
      app.service('authenticate').reauthenticate(accessToken)
   }
})


export default app
