
import { createRouter, createWebHistory } from 'vue-router'


async function checkAuth(to, from, next) {
   const accessToken = window.sessionStorage.getItem('feathers-jwt')
   console.log('checkAuth accessToken', accessToken)
   if (accessToken) {
      next()
   } else {
      next('/')
   }
}

const routes = [
   {
      path: '/',
      redirect: '/signin',
   },

   {
      path: '/signin',
      component: () => import('/src/views/Signin.vue'),
   },

   {
      path: '/signup',
      component: () => import('/src/views/Signup.vue'),
      props: true,
   },

   {
      path: '/home/:userId',
      component: () => import('/src/views/Home.vue'),
      props: true,
      beforeEnter: checkAuth,
      children: [
         {
            path: 'admin',
            component: () => import('/src/views/Admin.vue'),
         },
         {
            path: 'user',
            component: () => import('/src/views/User.vue'),
            props: true,
         },
      ],
   },

   {
      path: "/:catchAll(.*)",
      redirect: '/',
   },
]

const router = createRouter({
   history: createWebHistory(),
   routes
})

router.beforeEach(async (to, from, next) => {
   console.log('from', from.path, 'to', to.path)
   next()
})

export default router
