
# Run application in dev mode

## Clone project
```
git clone https://gitlab.com/buisson31/express-x-sampleapp
cd express-x-sampleapp
```

## Backend

### Install backend dependencies
```
cd backend; npm install
```

### Create database and configure its URL

- Create an empty  postgres/mysql/sqlite database
- Edit `DATABASE_URL` in `backend/.env`
- Run `npm run migrate` to create database tables from schema (in `backend/prisma/prisma.schema`)


### Create super-user
```
npm run createsuperuser
```

### Create sample groups
```
npm run creategroups
```

Add for example groups 'Gryffondor' and 'Serpentard'


### Check database with AdminJS
```
npm run admin
```
Open browser at http://localhost:3020/admin/resources/User


### Run backend
```
npm run dev
```

## Frontend

### Install frontend dependencies
```
cd frontend; npm install
```

### Run frontend dev server
```
npm run dev
```
Open browser at http://localhost:3000

Log-in with superuser credentials

